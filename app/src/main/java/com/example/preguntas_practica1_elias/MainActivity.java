package com.example.preguntas_practica1_elias;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.preguntas_practica1_elias.model.Preguntas;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText pregunta1,pregunta2,pregunta3,pregunta4,
             opcion1,opcion2,opcion3,opcion4,
             correcta1,correcta2,correcta3,correcta4,
             eje1,eje2,eje3,eje4;
    Button enviar;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pregunta1=findViewById(R.id.editText18);
        pregunta2=findViewById(R.id.editText19);
        pregunta3=findViewById(R.id.editText20);
        pregunta4=findViewById(R.id.editText21);
        opcion1=findViewById(R.id.editText23);
        opcion2=findViewById(R.id.editText24);
        opcion3=findViewById(R.id.editText25);
        opcion4=findViewById(R.id.editText26);
        enviar=findViewById(R.id.button);
        correcta1=findViewById(R.id.correcta1);
        correcta2=findViewById(R.id.correcta2);
        correcta3=findViewById(R.id.correcta3);
        correcta4=findViewById(R.id.correcta4);
        eje1=findViewById(R.id.latlong1);
        eje2=findViewById(R.id.latlong2);
        eje3=findViewById(R.id.latlong3);
        eje4=findViewById(R.id.latlong4);
        enviar.setOnClickListener(this);
        FirebaseApp.initializeApp(this);
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
    }

    @Override
    public void onClick(View v) {
if (opcion1.getText().toString().isEmpty() || opcion2.getText().toString().isEmpty()
        || opcion3.getText().toString().isEmpty()
        || opcion4.getText().toString().isEmpty()
        || pregunta1.getText().toString().isEmpty()
        || pregunta2.getText().toString().isEmpty()
        || pregunta3.getText().toString().isEmpty()
        || pregunta4.getText().toString().isEmpty()
        || correcta1.getText().toString().isEmpty()
        || correcta2.getText().toString().isEmpty()
        || correcta3.getText().toString().isEmpty()
        || correcta4.getText().toString().isEmpty()
        || eje1.getText().toString().isEmpty()
        || eje2.getText().toString().isEmpty()
        || eje3.getText().toString().isEmpty()
        || eje4.getText().toString().isEmpty()
){
    Toast.makeText(this, "Hay campos vacios", Toast.LENGTH_LONG).show();
}else{
    if (    opcion1.getText().toString().split(",").length<3
            ||eje1.getText().toString().split(",").length<1
            ||eje2.getText().toString().split(",").length<1
            ||eje3.getText().toString().split(",").length<1
            ||eje4.getText().toString().split(",").length<1
            ||Integer.parseInt(correcta1.getText().toString())>4
            ||Integer.parseInt(correcta2.getText().toString())>4
            ||Integer.parseInt(correcta3.getText().toString())>4
            ||Integer.parseInt(correcta4.getText().toString())>4
            || opcion2.getText().toString().split(",").length<3
            || opcion3.getText().toString().split(",").length<3
            || opcion4.getText().toString().split(",").length<3){

        Toast.makeText(this, "Las 4 opciones y las latitudes tienen que estar separadas por comas o numero de respuesta correcta mayor de 4", Toast.LENGTH_LONG).show();

    }else{
        Preguntas preguntas =new Preguntas();
        preguntas.setPregunta1(pregunta1.getText().toString());
        preguntas.setPregunta2(pregunta2.getText().toString());
        preguntas.setPregunta3(pregunta3.getText().toString());
        preguntas.setPregunta4(pregunta4.getText().toString());
        String[] opciones1 = opcion1.getText().toString().split(",");
        String[] opciones2 = opcion2.getText().toString().split(",");
        String[] opciones3 = opcion3.getText().toString().split(",");
        String[] opciones4 = opcion4.getText().toString().split(",");
        String[] auxLatLong1 = eje1.getText().toString().split(",");
        String[] auxLatLong2 = eje2.getText().toString().split(",");
        String[] auxLatLong3 = eje3.getText().toString().split(",");
        String[] auxLatLong4 = eje4.getText().toString().split(",");
        String[] letras= {String.valueOf('a'), String.valueOf('b'),String.valueOf('c'),String.valueOf('d')};
        preguntas.setLatitud1(Double.parseDouble(auxLatLong1[0]));
        preguntas.setLogintud1(Double.parseDouble(auxLatLong1[1]));
        preguntas.setLatitud2(Double.parseDouble(auxLatLong2[0]));
        preguntas.setLogintud2(Double.parseDouble(auxLatLong2[1]));
        preguntas.setLatitud3(Double.parseDouble(auxLatLong3[0]));
        preguntas.setLogintud3(Double.parseDouble(auxLatLong3[1]));
        preguntas.setLatitud4(Double.parseDouble(auxLatLong4[0]));
        preguntas.setLogintud4(Double.parseDouble(auxLatLong4[1]));
        preguntas.setCorrecta1(letras[Integer.parseInt(correcta1.getText().toString())-1]);
        preguntas.setCorrecta2(letras[Integer.parseInt(correcta2.getText().toString())-1]);
        preguntas.setCorrecta3(letras[Integer.parseInt(correcta3.getText().toString())-1]);
        preguntas.setCorrecta4(letras[Integer.parseInt(correcta4.getText().toString())-1]);
        preguntas.setRespuesta1a(opciones1[0]);
        preguntas.setRespuesta1b(opciones1[1]);
        preguntas.setRespuesta1c(opciones1[2]);
        preguntas.setRespuesta1d(opciones1[3]);
        preguntas.setRespuesta2a(opciones2[0]);
        preguntas.setRespuesta2b(opciones2[1]);
        preguntas.setRespuesta2c(opciones2[2]);
        preguntas.setRespuesta2d(opciones2[3]);
        preguntas.setRespuesta3a(opciones3[0]);
        preguntas.setRespuesta3b(opciones3[1]);
        preguntas.setRespuesta3c(opciones3[2]);
        preguntas.setRespuesta3d(opciones3[3]);
        preguntas.setRespuesta4a(opciones4[0]);
        preguntas.setRespuesta4b(opciones4[1]);
        preguntas.setRespuesta4c(opciones4[2]);
        preguntas.setRespuesta4d(opciones4[3]);

        Toast.makeText(this, "Se insertó en Firebase!", Toast.LENGTH_LONG).show();
        System.out.println("Holaaaa!!!");
        databaseReference.child("Preguntas").child(UUID.randomUUID().toString()).setValue(preguntas);
        opcion1.setText("");
        opcion2.setText("");
        opcion3.setText("");
        opcion4.setText("");
        pregunta1.setText("");
        pregunta2.setText("");
        pregunta3.setText("");
        pregunta4.setText("");
    }

}


    }
}
